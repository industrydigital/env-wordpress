FROM industrydigital/php-nginx:1.2.0

ARG DOCKER_HOST_UID=1000
ARG DOCKER_HOST_GID=1000
ARG WP_VERSION=5.2.2

COPY .docker/artifacts /tmp/artifacts

RUN set -xeu \
    && apt-get update \
    && apt-get install -y less \
    && usermod -u $DOCKER_HOST_UID www-data \
    && groupmod -g $DOCKER_HOST_GID www-data \
    && cd $RUNTIME_WEBROOT/.. \
    && rm -rf html \
    && tar -xzf "/tmp/artifacts/wordpress-${WP_VERSION}.tar.gz" \
    && mv wordpress html \
    && rm -f html/wp-config* \
    && cp /tmp/artifacts/wp-config.php html \
    && mkdir -p html/bin \
    && cp /tmp/artifacts/wp-cli.phar html/bin/wp \
    && chmod +x html/bin/wp \
    && chown -R root:www-data html \
    && chown -R www-data:www-data /run/php \
    && ls -l /run \
    && rm -rf /tmp/artifacts \
    && apt-get clean

VOLUME ["$RUNTIME_WEBROOT/wp-content"]
