<?php

////////////////
$env = getenv();
////////////////

define('DB_HOST',          $env['WP_DB_HOST']);
define('DB_NAME',          $env['WP_DB_NAME']);
define('DB_USER',          $env['WP_DB_USER']);
define('DB_PASSWORD',      $env['WP_DB_PASSWORD']);
define('DB_CHARSET',       $env['WP_DB_CHARSET'] ?? 'utf8');
define('DB_COLLATE',       $env['WP_DB_COLLATE'] ?? '');

define('AUTH_KEY',         $env['WP_AUTH_KEY']);
define('SECURE_AUTH_KEY',  $env['WP_SECURE_AUTH_KEY']); 
define('LOGGED_IN_KEY',    $env['WP_LOGGED_IN_KEY']); 
define('NONCE_KEY',        $env['WP_NONCE_KEY']); 
define('AUTH_SALT',        $env['WP_AUTH_SALT']); 
define('SECURE_AUTH_SALT', $env['WP_SECURE_AUTH_SALT']); 
define('LOGGED_IN_SALT',   $env['WP_LOGGED_IN_SALT']); 
define('NONCE_SALT',       $env['WP_NONCE_SALT']); 

$table_prefix = $env['WP_DB_TABLE_PREFIX'] ?? 'wp_';

define('WP_DEBUG', filter_var($env['WP_DEBUG'] ?? false, FILTER_VALIDATE_BOOLEAN));

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

////////////
unset($env);
////////////

require ABSPATH.'vendor/autoload.php';

require_once( ABSPATH . 'wp-settings.php' );

