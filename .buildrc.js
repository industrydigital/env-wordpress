// see http://devdocs.it-consultis.com.cn/npm/itc-package-builder/
// do NOT use ES6 module export syntax here
module.exports = {

  paths: {
    build: '.build',
    dist: 'dist',
  },

  // default directory and file perms
  dirmode: '0755',
  filemode: '0644',

  // gulp.src(source).pipe(gulp.dest(dest))
  // dest is a path relative to BUILD_INSTALL_PATH
  // source globs are relative to the project root
  files: [
    {
      dest: '',
      source: [
        '.env.default',
        'deploy/resources/artisan',
        'deploy/resources/generate-docker-compose',
        'deploy/resources/start',
        'mysql',
        'stop',
      ],
    },
    {
      dest: '.docker',
      source: [
        '.docker/**',
      ],
    },
    {
      dest: 'src',
      source: [
        'src/**',
        '!src/bootstrap/cache',
        '!src/bootstrap/cache/**',
        '!src/composer*',
        '!src/package.json.dist',
        '!src/phpunit*',
        '!src/readme.md',
        '!src/storage',
        '!src/storage/**',
        '!src/tests',
        '!src/tests/**',
        '!src/webpack.mix.js',
      ],
    },
  ],

  // ad-hoc perms that are assigned right before the package archive is created
  chmods: [
    ['0750', 'artisan'],
    ['0640', '.env.default'],
    ['0750', 'generate-docker-compose'],
    ['0750', 'mysql'],
    ['0750', 'src/artisan'],
    ['0750', 'start'],
    ['0750', 'stop'],
  ],

  // "debian", "rpm", "tarball", or "zipball"
  packaging: {
    type: 'debian',

    options: {
      scripts: {
        postinst: (prefix, build) => {
          return [
            '#!/usr/bin/env bash',
            'set -x',
            `cd ${prefix}`,
            './start',
          ];
        },
      },
      compression: {
        algo: 'bzip2',
        level: '9',
      },
    },
  },

  logrotate: {
    etc: '/etc/logrotate.d',
    stanzas: [
      {
        path: '.runtime/var/log/nginx/*.log',
        directives: [
          'daily',
          'missingok',
          'copytruncate',
          'compress',
          'notifempty',
          'create 640 root www-data',
        ],
      },
      {
        path: 'src/storage/logs/*.log',
        directives: [
          'daily',
          'missingok',
          'copytruncate',
          'compress',
          'notifempty',
          'create 640 root www-data',
        ],
      },
    ],
  },

};

// vim: syntax=javascript ts=2 sts=2 sw=2 et
