# Env-Laravel

## What is it?

This is a complete Laravel boilerplate project that provides:

- Dockerization via docker-compose
- nginx
- PHP 7.2
- Percona Server
- Redis
- [Imaginary](https://github.com/h2non/imaginary)
- Laravel 5.6
- Webpack development server
- Babel presets env
- SASS compilation
- Gulp build process that generates a Debian package
- Ansible deployment

## How to use this boilerplate:

- Clone this project
- Delete the `.git` directory
- Change the `PROJECT` variable inside `.env.default`
- Push the project to your remote repository.

## Notable source paths

Javascript belongs in `resources/assets/js`
SASS sources belong in `resources/assets/scss`

## How to run the project
```
./scripts/start
```

## Scripts you need to be aware of

The following scripts are in place to help you avoid problems with filesystem
permissions. They reside in the `scripts` directory. USE THEM.

```
start      starts the project
stop       tears down the project
composer   executes a composer process as YOU inside the buildbox container
artisan    executes an artisan process as YOU inside the buildbox container
gulp       executes a gulp process as YOU inside the buildbox container
npm        executes an npm process as YOU inside the buildbox container
```

## Environment Identifiers

```
local
staging
production
```

## How to build a package

After the docker containers are up:
```
./gulp build --env=$ENVIRONMENT
```

You should see Debian and tarball packages appear in the `dist` directory.

## How to deploy a package

Deployments are completely automated via GitLab CI. Please take a look at the
`.gitlab-ci.yml` file to see which branches deploy to which environments.


The following CI configuration triggers a deployment to staging when changes are
pushed to `develop` branch or any semantically-versioned release branch:

```
deploy-staging-job:
  stage: deploy
  only:
    - develop
    - /^release\/\d+\.\d+\.\d+$/
  tags:
    - buildbox-php:0.9.X
  script:
    - export ENVIRONMENT=staging
    - scripts/test
    - scripts/clean
    - scripts/build "$ENVIRONMENT"
    - scripts/deploy "$ENVIRONMENT" "$(ls dist/*.deb | head -n 1)"
```

## Contributing

Contributions to this project are welcome! Here's how you can help:

1. Fork `develop` branch.
1. Commit and push changes to your fork.
1. Create a merge request from your fork that points to our `develop` branch.

