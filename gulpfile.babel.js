import gulp from 'gulp';
import P from 'bluebird';
import _ from 'lodash';
import subprocess from 'child_process';
import fs from 'fs';
import path from 'path';
import minimist from 'minimist';
import handlebars from 'handlebars';
import {Project, Build, build} from 'itc-package-builder';

///////////////////////////////////////////////////////////////////////////////

const env = process.env;
const exec = P.promisify(subprocess.exec);
const readfile = P.promisify(fs.readFile);
const writefile = P.promisify(fs.writeFile);
const log = (...args) => console.log(...args);
const argv = minimist(process.argv.slice(2))

const PROJECT = argv.project || env.PROJECT || 'sandbox';
const ENVIRONMENT = argv.env || env.ENVIRONMENT || 'staging';
const PROJECT_ROOT = __dirname;

///////////////////////////////////////////////////////////////////////////////

/**
 * Read the contents of a file
 * @param {String} filepath
 * @return {String}
 */
const read = (filepath) => {
  return readfile(filepath).then(buffer => String(buffer).trim());
};

/**
 * Write the supplied buffer or string to a file
 * @param {String} filepath
 * @param {String|Buffer} buffer
 * @param {Object} options
 * @return void
 */
const write = (filepath, buffer, options) => {
  let defaults = {encoding: 'utf-8', mode: '0644', flag: 'w'};
  return writefile(filepath, buffer, _.defaults(options, defaults));
};

/**
 * @async Promise
 * @param {String} template
 * @param {Object} context
 * @return {String}
 */
const render_template = (template, context) => {
  return read(template).then((hbs) => handlebars.compile(hbs)(context));
};

/**
 * @async Promise
 * @param void
 * @return {String}
 */
const generate_nginx_vhost = () => {
  let local = ENVIRONMENT === 'local';
  let basename =  'nginx-vhost';
  let template = `${PROJECT_ROOT}/.docker/resources/${basename}.hbs`;

  return render_template(template, env);
};

/**
 * @param void
 * @return {Build}
 */
const get_build_context = () => {
  let project = new Project({
    name: PROJECT,
    root: __dirname,
    maintainer: 'IT Consultis <tech@it-consultis.com>',
    repository: `ssh://git@git.it-consultis.net:22080/itc/${PROJECT}.git`,
  });

  return Build.preset('default', {project, environment: ENVIRONMENT, log});
};

////////////////////////////////////////////////////////////////////////////

const tasks = {

  /**
   * Remove build artifacts
   * @async Promise
   * @param void
   * @return void
   */
  clean: () => {
    let buildctx = get_build_context();
    return buildctx.clean();
  },

  /**
   * Select an nginx virtual host handlebars template, then render it, supplying
   * environment variables as the template context.
   * @async Promise
   * @param void
   * @return void
   */
  nginx: () => {
    let output_dir = `${PROJECT_ROOT}/.docker/environments/all/etc/nginx/sites-enabled`;
    let output_path = `${output_dir}/default`;

    return exec(`mkdir -p ${output_dir}`)
    .then(generate_nginx_vhost)
    .then((vhost) => write(output_path, vhost))
  },

  /**
   * Collect files in the build directory, then generate a Debian package
   * in the dist directory.
   * @async Promise
   * @param void
   * @return void
   */
  build: () => {
    let buildctx = get_build_context();

    buildctx.add(new build.steps.AdHoc({
      messages: {execute: 'Creating nginx configuration'},
      execute: tasks.nginx,
    }));

    buildctx.add(new build.steps.AdHoc({
      messages: {execute: 'Installing Composer dependencies'},
      execute: () => exec('cd src && composer -vvv install --no-scripts --no-dev --ignore-platform-reqs'),
    }));

    buildctx.add(new build.steps.AdHoc({
      messages: {execute: 'Running artisan optimize command'},
      execute: () => exec('cd src && ./artisan optimize'),
    }));

    buildctx.add(new build.steps.AdHoc({
      messages: {execute: 'Installing frontend dependencies'},
      execute: () => exec('npm install'),
    }));

    buildctx.add(new build.steps.AdHoc({
      messages: {execute: 'Building frontend artifacts'},
      execute: () => exec('npm run build'),
    }));

    return buildctx.execute()

    .catch(e => {
      log('Build failed');
      log(e.message);
      log(e.stack);
      process.exit(1);
    });

  }, // pkg()

  /**
   * Build a package, then deploy it with Ansible
   * @async
   * @param void
   * @return void
   */
  deploy: () => {
    // run clean and build tasks
    return tasks.clean().then(tasks.build)

    // delegate to the deploy script
    .then((pkgpath) => {
      let script = 'scripts/deploy';
      let stdio = 'inherit';

      let cp = subprocess.spawn(script, [ENVIRONMENT, pkgpath], {stdio});

      return new P((resolve, reject) => {
        cp.on('error', reject);
        cp.on('exit', (code) => code === 0 ? resolve() : reject());
      });
    })

  }, // deploy()

}

///////////////////////////////////////////////////////////////////////////

gulp.task('clean', tasks.clean);
gulp.task('generate-nginx-vhost', tasks.nginx);
gulp.task('build', ['clean'], tasks.build);
gulp.task('deploy', ['clean'], tasks.deploy);
gulp.task('default', tasks.build);

